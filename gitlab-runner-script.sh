#!/bin/bash
#Runner user script
#put this script in optional script area while creating the ubuntu image in aws

#install runner
sudo apt-get update -y 
sudo curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash 
sudo apt-get install gitlab-runner -y

#install nodejs
curl -sL https://deb.nodesource.com/setup_14.x -o nodesource_setup.sh 
sudo bash nodesource_setup.sh
sudo apt-get install -y nodejs 
sudo npm install forever -g

#install docker 
curl -fsSL https://get.docker.com -o get-docker.sh 
sudo bash get-docker.sh
cd /var/run/ 
sudo chmod 777 docker.pid docker.sock

#install aws cli 
sudo apt-get update -y 
sudo apt-get install awscli zip -y
